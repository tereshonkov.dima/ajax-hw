const wrapper = document.getElementById("wrapper");
function getFilms() {
    let xhr = new XMLHttpRequest();
    xhr.open("GET", "https://ajax.test-danit.com/api/swapi/films");
    xhr.onreadystatechange = function () {
    if(xhr.readyState === 4) {
        films = JSON.parse(xhr.response);
        console.log(films);
        if(xhr.status === 200) {
            displayFilm(films);
        }
    }
}
xhr.send();
};
getFilms();

function displayFilm(objs) {
    for(const obj of objs) {
        const ul = document.createElement("ul");
        const li = document.createElement("li");
        
        const nameH2 = document.createElement("h2");
        nameH2.textContent = `Назва фільму ${obj.name}`;
        li.append(nameH2);

        const actors = document.createElement("ul");
        actors.textContent = `Персонажі:`;
        li.append(actors);
    
        const paragraphEpisode = document.createElement("p");
        paragraphEpisode.textContent = `Номер епізоду ${obj.episodeId}`;
        li.append(paragraphEpisode);
    
        const paragraphCrawl = document.createElement("p");
        paragraphCrawl.textContent = `Короткий опис фільму ${obj.openingCrawl
        }`;
        li.append(paragraphCrawl);
    
        ul.append(li);
        wrapper.append(ul); 
        updatePersFilm(obj.characters, actors);
    }
};

function updatePersFilm (filmUrl, actors) {
    filmUrl.forEach(url => {
        fetch(url)
        .then(response => response.json())
        .then(actor => {
            console.log(actor);
            const actorLi = document.createElement("li");
            actorLi.textContent = `Імʼя ${actor.name}, Дата народження ${actor.birthYear
            || "Невідомо"}, Пол ${actor.gender}, Волосся ${actor.hairColor
            }, `;
            actors.append(actorLi);
        })
    })
};